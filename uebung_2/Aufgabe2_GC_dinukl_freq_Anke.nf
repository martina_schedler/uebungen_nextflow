// Eingabe: FASTA-Datei 
// GC content berechnen: (G+C) / (A+C+G+T)
// Zusatzaufgabe: Dinukleotidfrequenzen berechnen 
// Zusatzaufgabe: Gesamt-Ausgabedatei mit GC content und Dinukleotidfrequenzen untereinander
nextflow.enable.dsl=2

process read_fasta{
    publishDir "/tmp/fastaGC", mode: 'copy', overwrite: true // die ergebnisse vom output werden hier hin kopiert
    input:
        path infile // ( wird in Zeile 34 angegeben; nicht in Konoslenbefehl hier)
    output:
        path "${infile}.line*", emit: eingabedatei 
    script: // alles hier und folgende zeile ist bash /* mit d zeichen kann man auf variable greifen. */ ; jeweils 2 Zeilen gesplittet
        """
        split -l 2 ${infile} -d ${infile}.line 
        """
}

process content_GC {
  publishDir params.outdir, mode: 'copy', overwrite: true // --outdir muss in Konsolenbefehl rein!
  input:
    path infile
  output:
    path "${infile}.content_GC", emit: GCcontent
  script: //Python Skript übergeben; dahinter steht die Eingabedatei (${infile}) die aus read_fasta kommt; das was da rauskommt soll dann ausgebeben werden als ${infile}.content_GC
    """
    python3 ~/ngsmodule/cq-init/uebungen/nextflow_uebung/contentGC_Anke.py ${infile} > ${infile}.content_GC
    """
}



workflow {
    inchannel = channel.fromPath("../data/sequences.fasta") // muss nicht mehr in Konsolenbefehl bei --infile angegeben werden
    splitfiles = read_fasta(inchannel) // name des ersten prozesses
    GC_content = content_GC(splitfiles.flatten())
    //stopcounts = count_Stop(splitfiles.flatten())
    //allStops = sumAllStops(stopcount.collect())
}





nextflow.enable.dsl=2

process split_file {
  input:
    path infile
  output:
    path "${infile}.line*"
  script: // split -l 2 damit immer in 2 Zeilen-Abschnitte gesplittet --> 4 Dateien; -d ist der Prefix also der Anfang der Dateiname
    """
    split -l 2 ${infile} -d ${infile}.line
    """
}

process count_ATG {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infile
  output:
    path "${infile}.ATGcount", emit: ATGcount 
  script: // tail -n 1 nimmt letzte Zeile und packt in sequenz, grep nimmt daraus ATGs und packt in grepresult; cat schreibt alle ATGs untereinander; wc -l: nicht Wörter sondern Zeilen zählen durch -l und in AusgabeDatei packen
    """
    tail -n 1 ${infile} > sequenz
    grep -o -i "ATG" sequenz > grepresult
    cat grepresult | wc -l > ${infile}.ATGcount 
    """
}

process count_STOP {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infile
  output:
    path "${infile}.STOPcount", emit: STOPcount
  script: // TAA oder TAG oder TGA weil pipes in '' bei egrep
    """
    tail -n 1 ${infile} > sequenz
    egrep -o -i 'TAA|TAG|TGA' sequenz > grepresult
    cat grepresult | wc -l >> ${infile}.STOPcount
    """
}

process calculate_total_ATG {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infiles
  output:
    path "ATGcount" // Name der Ausgabedatei
  script: // cat schreibt die Zahlen aus den Ausgabedatein untereinander; paste -sd fügt Pluse dawzischen ein 1+1+1+1...; bc ist integrierter Taschenrechner der rechnet 1+1+1+1.. zusammen
    """
    cat ${infiles} | paste -sd "+" | bc > ATGcount
    """
}

process calculate_total_STOP {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infiles
  output:
    path "STOPcount"
  script:
    """
    cat ${infiles} | paste -sd "+" | bc > STOPcount
    """
}

process out {
  publishDir params.outdir, mode: 'copy', overwrite: true // -- outdir muss in Konsole angegeben werden
  input:
    path starts
    path stops
  output:
    path "answer"
  script:
    """
    echo -n  "Die Anzahl der Startcodons beträgt: " > answer
    cat ${starts} >> answer
    echo -n  "Die Anzahl der Stopcodons beträgt: " >> answer
    cat ${stops} >> answer
    """
}


workflow {
/* // wir erstellen uns neue Datei aus Dateinamen und fragen ob sie existiert; wenn nicht existiert dann beende dich
  if(!file(params.infile).exists()) {
    println("Input file ${params.infile} does not exist.")
    exit(1)
  }*/
  inchannel = channel.fromPath(params.infile) //--infile Fasta Datei muss in die Konsole
  splitfiles = split_file(inchannel) // das was der Prozess split_file zurückgibt kommt in den Kanal splitfiles
  startcounts = count_ATG(splitfiles.flatten()) // Kanal startcounts, Prozess count_ATG; flatten von Splitfiles damit die Datein einzeln rausgenommen werden 
  stopcounts = count_STOP(splitfiles.flatten()) // wie oben
  starts = calculate_total_ATG(startcounts.collect()) // Kanal starts, Prozess starts, Collect um Datein zusammenzupacken
  stops = calculate_total_STOP(stopcounts.collect()) // wie oben
  out(starts,stops) 

}




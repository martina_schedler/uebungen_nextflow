//uebung
//input: fasta: 
//output: länge der sequenzen 

nextflow.enable.dsl = 2

process split_file {

    input:
        path infile
    output:
        path "${infile}.line*"
    script:
    """
    split -l 2 ${infile} -d ${infile}.line
    """
    
}


process calc_length{
//berechnet längen

    input:
        path seqfile
    output:
        path "${seqfile}.laenge"
    script:
    """
    tail -n 1 ${seqfile} > sequenz
    cat sequenz | tr -d "\n" | wc -m > ${seqfile}.laenge
    """

}


process summary{
    publishDir "${params.outdir}", mode: 'copy', overwrite: true

    input:
        path lenfiles
    output:
        path "lenout"
    script:
    """
    cat ${lenfiles} > lenout
    """

}


workflow {

//einlesen
inchannel = channel.fromPath(params.infile)
//split_file
splitfiles = split_file(inchannel)
//länge bestimmen
laengen = calc_length(splitfiles.flatten())
//zusammenfassung ergebnisse
ergebnisse = summary(laengen.collect())
}
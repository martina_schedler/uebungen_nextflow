// Eingabe: FASTA-Datei 
// Ausgabe: Datei mit Längen der Sequenzen

nextflow.enable.dsl=2

process read_fasta{
    publishDir "/tmp/fastaleange", mode: 'copy', overwrite: true // die ergebnisse vom output werden hier hin kopiert
    input:
        path infile // angeben bei inchannel im workflow, nicht im Konsolenbefehl
    output:
        path "${infile}.line*", emit: eingabedatei 
    script: // jeweils 2 Zeilen gesplittet
        """
        split -l 2 ${infile} -d ${infile}.line 
        """
}

process calc_length { //Länge der Sequenz berechnen
  publishDir params.outdir, mode: 'copy', overwrite: true // --outdir muss in Konsolenbefehl rein!
  input:
    path seqfile
  output:
    path "${seqfile}.sequence_length", emit: lengthSequence
  script: // tail -n1 nimmt untere Zeile mit Sequenz -> in sequenz reinschieben; cat gibt sequenz wieder; tr löscht newline am Ende der Sequzenz damit er nicht ein Zeichen zu viel zählt; wc -m zählt die Zeichen ; Ausgabe in Datein ${infile}.sequence_length (; weglassen: grep -v 'Y' sequenz > grepresult )
    """
    tail -n 1 ${seqfile} > sequenz
    cat sequenz |tr -d "\n"| wc -m > ${seqfile}.sequence_length
    """
}


process summary {
    // wenn man hier kein publishDir angibt, muss man am Ende in Konosle eigeben: cat work/den 2stelligen Code der vor process >summary steht in eckigen Klammern/ den Code der dannach steht/lenout
  publishDir params.outdir, mode: 'copy', overwrite: true // --outdir muss in Konsolenbefehl rein!
  input:
    path lenfiles
  output:
    path "lenout"
  script: // cat gibt Inhalte der Datein aus in lenout c 
    """
    cat ${lenfiles} > lenout
    """
}


workflow {
    inchannel = channel.fromPath("../data/sequences.fasta") // Eingabedatei einlesn; muss nicht mehr in Konsolenbefehl bei --infile angegeben werden
    splitfiles = read_fasta(inchannel) // Splitten
    laengen = calc_length(splitfiles.flatten()) // Länge bestimmen; flatten damit viele Datein aus dem Splitten (kommt als Array raus) einzeln im Channel sind fürs Längen bestimmen )
    Ergebnisse = summary(laengen.collect())// Zusammenfassung der Ergebnisdatein
}

